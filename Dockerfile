﻿FROM mcr.microsoft.com/dotnet/sdk:7.0-alpine as base
WORKDIR /app
COPY . .

RUN dotnet restore --configfile ./Nuget.Config
RUN dotnet build --no-restore
RUN dotnet publish -o /app/publish

FROM mcr.microsoft.com/dotnet/aspnet:7.0-alpine
WORKDIR /app
COPY --from=base /app/publish ./

ENV ASPNETCORE_HTTP_PORT=80 
ENV ASPNETCORE_DEBUG_PORT=84
EXPOSE 80
EXPOSE 84
ENTRYPOINT ["dotnet", "/app/Promocodeaggregator.Polling.dll"]
