using Hangfire;
using Hellang.Middleware.ProblemDetails;
using Microservices.Kafka.Extensions;
using PromocodeAggregator.Polling.Extension;
using PromocodeAggregator.Polling.FIlter;
using PromocodeAggregator.Polling.Options;
using PromocodeAggregator.Polling.Services.HangfireExecutor;
using PromocodeAggregator.Polling.Services.Scheduler;

namespace PromocodeAggregator.Polling;

public class Startup
{
	private readonly IConfiguration _configuration;

	public Startup(IConfiguration configuration)
	{
		_configuration = configuration;
	}

	public void ConfigureServices(IServiceCollection services)
	{
		services
			.AddProblemDetails(options => options.MapToStatusCode<Exception>(StatusCodes.Status500InternalServerError))
			.AddMediator(options => options.ServiceLifetime = ServiceLifetime.Scoped)
			.AddHangfire(_configuration);

		services
			.Configure<HabrPollingOptions>(_configuration.GetRequiredSection(nameof(HabrPollingOptions)).Bind)
			.Configure<PromocodusOptions>(_configuration.GetRequiredSection(nameof(PromocodusOptions)).Bind)
			.Configure<LifehackerOptions>(_configuration.GetRequiredSection(nameof(LifehackerOptions)).Bind)
			.Configure<ApplicationProducerOptions>(_configuration.GetRequiredSection(nameof(ApplicationProducerOptions)).Bind);

		services
			.AddKafkaProducer(_configuration.GetRequiredSection("KafkaOptions").Bind)
			.AddKafkaSystemTextJsonSerializers();

		services.AddHostedService<HangfireScheduler>();
		services
			.AddSingleton<IHangfireExecutor, HangfireExecutor>();

		services.AddControllers();
	}

	public void Configure(IApplicationBuilder app)
	{
		app.UseProblemDetails();

		app.UseRouting();

		app.UseEndpoints(endpoints => endpoints.MapControllers());

		app.UseHangfireDashboard(options: new DashboardOptions
		{
			DisplayNameFunc = (_, job) => job.Type switch
			{
				_ => job.Method.Name
			},
			Authorization = new[] { new NoDashboardAuthorizationFilter() }
		});
	}
}
