﻿using Hangfire.Dashboard;

namespace PromocodeAggregator.Polling.FIlter;

public class NoDashboardAuthorizationFilter : IDashboardAuthorizationFilter
{
	public bool Authorize(DashboardContext context) => true;
}
