﻿using HtmlAgilityPack;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Promocodus;

public record PollPromocodusCategoriesRequest : IRequest<List<CategoryMessageModel>>;

public class PollPromocodusCategoriesRequestHandler :
	IRequestHandler<PollPromocodusCategoriesRequest, List<CategoryMessageModel>>
{
	private readonly IOptionsSnapshot<PromocodusOptions> _snapshot;

	public PollPromocodusCategoriesRequestHandler(IOptionsSnapshot<PromocodusOptions> snapshot)
	{
		_snapshot = snapshot;
	}

	public async ValueTask<List<CategoryMessageModel>> Handle(
		PollPromocodusCategoriesRequest request,
		CancellationToken cancellationToken)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();
		var uri = new Uri(new Uri(_snapshot.Value.BaseUrl), "categories");
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, uri), cancellationToken);
		var stringContent = await html.Content.ReadAsStringAsync(cancellationToken);
		document.LoadHtml(stringContent);

		var categories = document
			.DocumentNode
			.SelectNodes("//a[starts-with(@data-category, 'category')]")
			?.Select(x => new CategoryMessageModel
			{
				Name = new Uri(x.GetAttributeValue("href", null)).Segments.Last(),
				Description = x.ChildNodes
					.LastOrDefault(y => !string.IsNullOrWhiteSpace(y.InnerText))?
					.GetDirectInnerText().Trim(),
				Source = x.GetAttributeValue("href", null)
			})
			.ToList();

		return categories ?? new List<CategoryMessageModel>();
	}
}
