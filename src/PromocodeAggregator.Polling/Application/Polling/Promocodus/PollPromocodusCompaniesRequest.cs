﻿using HtmlAgilityPack;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Promocodus;

public record PollPromocodusCompaniesRequest(CategoryMessageModel Category) : IRequest<List<CompanyMessageModel>>;

public class PollPromocodusCompaniesRequestHandler :
	IRequestHandler<PollPromocodusCompaniesRequest, List<CompanyMessageModel>>
{
	private readonly IOptionsMonitor<PromocodusOptions> _monitor;

	public PollPromocodusCompaniesRequestHandler(IOptionsMonitor<PromocodusOptions> monitor)
	{
		_monitor = monitor;
	}

	public async ValueTask<List<CompanyMessageModel>> Handle(
		PollPromocodusCompaniesRequest request,
		CancellationToken cancellationToken)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();

		var uri = new Uri(new Uri(_monitor.CurrentValue.BaseUrl), $"categories/{request.Category.Name}");
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, uri), cancellationToken);
		var stringContent = await html.Content.ReadAsStringAsync(cancellationToken);
		document.LoadHtml(stringContent);

		var companies = document
			.DocumentNode
			.SelectNodes("//div[contains(@class, 'campaigns-no-img')]")
			?.FirstOrDefault()
			?.ChildNodes.Where(x => x.Name == "a").Select(x => new CompanyMessageModel
			{
				Name = x.GetDirectInnerText(),
				ImageLink = null,
				ImageBase64 = null,
				Source = x.GetAttributeValue("href", null),
				Category = request.Category
			})
			.Where(x => x.Source != null && x.Name != null)
			.ToList();

		return companies;
	}
}
