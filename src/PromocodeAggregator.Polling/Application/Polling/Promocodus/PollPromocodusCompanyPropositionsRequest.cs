﻿using HtmlAgilityPack;
using Mediator;
using Microservices.Kafka.Abstractions;
using Microservices.Kafka.Transfers;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Application.Polling.Abstractions;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Promocodus;

public record PollPromocodusCompanyPropositionsRequest(CompanyMessageModel CompanyMessage) : IRequest;

public class PollPromocodusCompanyPropositionsRequestHandler :
	PollBase,
	IRequestHandler<PollPromocodusCompanyPropositionsRequest>
{
	private readonly ILogger<PollPromocodusCompanyPropositionsRequest> _logger;
	private readonly IKafkaProducer<EventKey, PollingResultMessageModel> _producer;
	private readonly IOptionsMonitor<PromocodusOptions> _monitor;
	private readonly ApplicationProducerOptions _options;

	public PollPromocodusCompanyPropositionsRequestHandler(
		ILogger<PollPromocodusCompanyPropositionsRequest> logger,
		IKafkaProducer<EventKey, PollingResultMessageModel> producer,
		IOptionsMonitor<PromocodusOptions> monitor,
		IOptions<ApplicationProducerOptions> options)
	{
		_logger = logger;
		_producer = producer;
		_monitor = monitor;
		_options = options.Value;
	}

	public async ValueTask<Unit> Handle(
		PollPromocodusCompanyPropositionsRequest request,
		CancellationToken cancellationToken)
	{
		await LoadPromoCodes(request.CompanyMessage);

		return Unit.Value;
	}

	private async Task LoadPromoCodes(CompanyMessageModel company)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();

		var companyUrl = company.Source;
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, companyUrl));
		var stringContent = await html.Content.ReadAsStringAsync();
		document.LoadHtml(stringContent);

		var coupons = document.DocumentNode
			.SelectNodes(
				"//div[contains(@class, 'coupon') and not(contains(@class, 'cross'))]//div[contains(@class, 'coupon') and contains(@class, 'coupon-horizontal') and not(contains(@class, 'cross'))]")
			?.ToArray();

		var links = coupons?
			.Select(x =>
				x.SelectSingleNode(x.XPath + "//div[contains(@class,'logo-wrap')]//a")?.GetAttributeValue("href", null)
			).Where(x => x != null)
			.ToList();

		if (links is not [_, ..])
		{
			_logger.LogWarning("No promotion links for company {companyUrl}", companyUrl);
			return;
		}

		foreach (var link in links)
		{
			var couponId = new Uri(link).Segments.Last();

			var codeUrl = companyUrl + $"?couponId={couponId}";
			var codeResponse = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, codeUrl));
			var codeHtml = await codeResponse.Content.ReadAsStringAsync();

			document.LoadHtml(codeHtml);

			var propositionValue = GetPromocode(document);
			var propositionKind = PropositionKind.Promocode;

			if (string.IsNullOrWhiteSpace(propositionValue))
			{
				propositionKind = PropositionKind.Promotion;
				propositionValue = GetPromotion(document);
			}

			if (string.IsNullOrWhiteSpace(propositionValue))
			{
				_logger.LogWarning("Can't load propositon for {codeUrl}", codeUrl);
				await Task.Delay(_monitor.CurrentValue.PromotionInterval);
				continue;
			}

			var propositionItem = new PropositionItemMessageModel
			{
				Name = GetName(document),
				Value = propositionValue,
				Link = codeUrl,
				Description = propositionKind is not PropositionKind.Promotion ? GetDescription(document) : null,
				Expiration = GetExpiration(document),
				PropositionKind = propositionKind
			};

			var resultMessage = new PollingResultMessageModel
			{
				Company = company,
				Propositions = new List<PropositionItemMessageModel> { propositionItem },
				SourceId = (int)Sources.Promocodus
			};
			var key = new EventKey { Id = Guid.NewGuid() };
			await _producer.Produce(_options.PropositionsResultTopic, key, resultMessage);

			await Task.Delay(_monitor.CurrentValue.PromotionInterval);
		}
	}

	private DateTimeOffset? GetExpiration(HtmlDocument document)
	{
		var textValue = document.DocumentNode
			.SelectSingleNode("//div[@id='coupon-modal']//time[@class='time']")
			?.GetDirectInnerText();
		return ParseExpiration(textValue);
	}

	private static string GetDescription(HtmlDocument document) => RemoveExtraSpaces(document.DocumentNode
		.SelectSingleNode("//div[@class='cm-descr cm-visible']")?.GetDirectInnerText().Trim());

	private static string GetName(HtmlDocument document) => RemoveExtraSpaces(document.DocumentNode
		.SelectSingleNode("//h4[contains(@class, 'title')]")?.GetDirectInnerText().Trim());

	private static string GetPromotion(HtmlDocument document) => RemoveExtraSpaces(document.DocumentNode
		.SelectSingleNode("//div[@class='cm-descr cm-visible']")?.GetDirectInnerText().Trim());

	private static string GetPromocode(HtmlDocument document) => RemoveExtraSpaces(document.DocumentNode
		.SelectSingleNode("//div[contains(@class, 'code-wrap')]//input")?.GetAttributeValue("value", null)?.Trim());
}
