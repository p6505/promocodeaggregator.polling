﻿using System.Runtime.CompilerServices;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Extension;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Promocodus;

public record PollPromocodusPropositionsRequest : IRequest;

public class PollPromocodusPropositionsRequestHandler : IRequestHandler<PollPromocodusPropositionsRequest>
{
	private const Sources Source = Sources.Promocodus;
	
	private readonly ILogger<PollPromocodusPropositionsRequestHandler> _logger;
	private readonly IOptionsMonitor<PromocodusOptions> _monitor;
	private readonly IMediator _mediator;

	public PollPromocodusPropositionsRequestHandler(
		ILogger<PollPromocodusPropositionsRequestHandler> logger,
		IOptionsMonitor<PromocodusOptions> monitor,
		IMediator mediator)
	{
		_logger = logger;
		_monitor = monitor;
		_mediator = mediator;
	}

	public async ValueTask<Unit> Handle(PollPromocodusPropositionsRequest request, CancellationToken cancellationToken)
	{
		_logger.LogSourcePollingStarted(Source);
		var random = new Random();

		await foreach (var companies in GetCompanies(cancellationToken))
		{
			foreach (var company in companies.OrderBy(_ => random.Next()))
			{
				_logger.LogCompanyPollingStarted(Source, company.Category.Name, company.Name);

				await _mediator.Send(new PollPromocodusCompanyPropositionsRequest(company), cancellationToken);

				_logger.LogCompanyPollingFinished(Source, company.Category.Name, company.Name);

				await Task.Delay(_monitor.CurrentValue.CompanyInterval, cancellationToken);
			}
		}

		_logger.LogSourcePollingFinished(Source);

		return Unit.Value;
	}

	private async IAsyncEnumerable<List<CompanyMessageModel>> GetCompanies(
		[EnumeratorCancellation] CancellationToken cancellationToken)
	{
		Random random = new();

		var categories = await _mediator.Send(new PollPromocodusCategoriesRequest(), cancellationToken);
		var validCategories = categories
			.Where(x => _monitor.CurrentValue.IgnoredCategories
				.All(y => !string.Equals(y, x.Name, StringComparison.InvariantCultureIgnoreCase)))
			.OrderBy(_ => random.Next())
			.ToArray();

		foreach (var category in validCategories)
		{
			var companies = await _mediator.Send(new PollPromocodusCompaniesRequest(category), cancellationToken);
			_logger.LogCompaniesReceived(Source, companies.Count, category.Name);

			yield return companies;
		}
	}
}
