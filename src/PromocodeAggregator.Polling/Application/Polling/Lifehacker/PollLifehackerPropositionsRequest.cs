﻿using System.Runtime.CompilerServices;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Extension;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Lifehacker;

public record PollLifehackerPropositionsRequest : IRequest;

public class PollLifehackerPropositionsRequestHandler : IRequestHandler<PollLifehackerPropositionsRequest>
{
	private const Sources Source = Sources.Lifehacker;

	private readonly IMediator _mediator;
	private readonly IOptionsMonitor<LifehackerOptions> _monitor;
	private readonly ILogger<PollLifehackerPropositionsRequestHandler> _logger;

	public PollLifehackerPropositionsRequestHandler(
		IMediator mediator,
		IOptionsMonitor<LifehackerOptions> monitor,
		ILogger<PollLifehackerPropositionsRequestHandler> logger)
	{
		_mediator = mediator;
		_monitor = monitor;
		_logger = logger;
	}

	public async ValueTask<Unit> Handle(PollLifehackerPropositionsRequest request, CancellationToken cancellationToken)
	{
		_logger.LogSourcePollingStarted(Source);
		var random = new Random();

		await foreach (var companies in GetCompanies(cancellationToken))
		{
			foreach (var company in companies.OrderBy(_ => random.Next()))
			{
				_logger.LogCompanyPollingStarted(Source, company.Category.Name, company.Name);

				await _mediator.Send(new PollLifehackerCompanyPropositionsRequest(company), cancellationToken);

				_logger.LogCompanyPollingFinished(Source, company.Category.Name, company.Name);

				await Task.Delay(_monitor.CurrentValue.CompanyInterval, cancellationToken);
			}
		}

		_logger.LogSourcePollingFinished(Source);

		return Unit.Value;
	}

	private async IAsyncEnumerable<List<CompanyMessageModel>> GetCompanies(
		[EnumeratorCancellation] CancellationToken cancellationToken)
	{
		Random random = new();

		var categories = await _mediator.Send(new PollLifehackerCategoriesRequest(), cancellationToken);
		var validCategories = categories
			.Where(x => _monitor.CurrentValue.IgnoredCategories
				.All(y => !string.Equals(y, x.Name, StringComparison.InvariantCultureIgnoreCase)))
			.OrderBy(_ => random.Next())
			.ToArray();

		foreach (var category in validCategories)
		{
			var companies = await _mediator.Send(new PollLifehackerCompaniesRequest(category), cancellationToken);
			_logger.LogCompaniesReceived(Source, companies.Count, category.Name);

			yield return companies;
		}
	}
}
