﻿using HtmlAgilityPack;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Lifehacker;

public record PollLifehackerCategoriesRequest : IRequest<List<CategoryMessageModel>>;

public class PollPromocodusCategoriesRequestHandler :
	IRequestHandler<PollLifehackerCategoriesRequest, List<CategoryMessageModel>>
{
	private readonly IOptionsMonitor<LifehackerOptions> _monitor;

	public PollPromocodusCategoriesRequestHandler(IOptionsMonitor<LifehackerOptions> monitor)
	{
		_monitor = monitor;
	}

	public async ValueTask<List<CategoryMessageModel>> Handle(
		PollLifehackerCategoriesRequest request,
		CancellationToken cancellationToken)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();
		var uri = new Uri(new Uri(_monitor.CurrentValue.BaseUrl), "categories");
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, uri), cancellationToken);
		var stringContent = await html.Content.ReadAsStringAsync(cancellationToken);
		document.LoadHtml(stringContent);

		var categories = document
			.DocumentNode
			.SelectNodes("//a[@data-category]")
			?.Select(x => new CategoryMessageModel
			{
				Name = new Uri(x.GetAttributeValue("href", null)).Segments.Last(),
				Description = x.ChildNodes
					.LastOrDefault(y => !string.IsNullOrWhiteSpace(y.InnerText))?
					.GetDirectInnerText().Trim(),
				Source = x.GetAttributeValue("href", null)
			})
			.ToList();

		return categories ?? new List<CategoryMessageModel>();
	}
}
