﻿using HtmlAgilityPack;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Lifehacker;

public record PollLifehackerCompaniesRequest(CategoryMessageModel Category) : IRequest<List<CompanyMessageModel>>;

public class PollLifehackerompaniesRequestHandler :
	IRequestHandler<PollLifehackerCompaniesRequest, List<CompanyMessageModel>>
{
	private readonly IOptionsMonitor<LifehackerOptions> _monitor;

	public PollLifehackerompaniesRequestHandler(IOptionsMonitor<LifehackerOptions> monitor)
	{
		_monitor = monitor;
	}

	public async ValueTask<List<CompanyMessageModel>> Handle(
		PollLifehackerCompaniesRequest request,
		CancellationToken cancellationToken)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();

		var uri = new Uri(new Uri(_monitor.CurrentValue.BaseUrl), $"categories/{request.Category.Name}");
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, uri), cancellationToken);
		var stringContent = await html.Content.ReadAsStringAsync(cancellationToken);
		document.LoadHtml(stringContent);

		var companies = document
			.DocumentNode
			.SelectNodes("//div[contains(@class, 'campaigns-img')]//div[contains(@class, 'ca-col')]//a")
			?.Select(x => new CompanyMessageModel
			{
				Name = x.ChildNodes
					.LastOrDefault(y => !string.IsNullOrWhiteSpace(y.InnerText))?
					.GetDirectInnerText().Trim(),
				ImageLink = null,
				ImageBase64 = null,
				Source = x.GetAttributeValue("href", null),
				Category = request.Category
			});

		return companies
			?.Where(x => x.Source != null && x.Name != null)
			.ToList() ?? new List<CompanyMessageModel>();
	}
}
