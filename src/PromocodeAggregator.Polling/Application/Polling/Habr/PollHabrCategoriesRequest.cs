﻿using HtmlAgilityPack;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Habr;

public record PollHabrCategoriesRequest : IRequest<List<CategoryMessageModel>>;

/*
<a href="https://promo.habr.com/categories/razvlecheniya">
    <i class="icon-Emoji1"></i>
	Отдых и развлечения
</a>
 */
public class PollHabrCategoriesRequestHandler : IRequestHandler<PollHabrCategoriesRequest, List<CategoryMessageModel>>
{
	private readonly IOptionsMonitor<HabrPollingOptions> _monitor;

	public PollHabrCategoriesRequestHandler(IOptionsMonitor<HabrPollingOptions> monitor)
	{
		_monitor = monitor;
	}

	public async ValueTask<List<CategoryMessageModel>> Handle(
		PollHabrCategoriesRequest request,
		CancellationToken cancellationToken)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();
		var uri = new Uri(_monitor.CurrentValue.BaseUrl);
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, uri), cancellationToken);
		var stringContent = await html.Content.ReadAsStringAsync(cancellationToken);
		document.LoadHtml(stringContent);

		var categories = document
			.DocumentNode
			.SelectNodes("//ul[contains(@class,'m-dropdown')]//a")
			?.Select(x => new CategoryMessageModel
			{
				Name = new Uri(x.GetAttributeValue("href", null)).Segments.Last(),
				Description = x.ChildNodes
					.LastOrDefault(y => !string.IsNullOrWhiteSpace(y.InnerText))?
					.GetDirectInnerText().Trim(),
				Source = x.GetAttributeValue("href", null)
			})
			.ToList();

		return categories ?? new List<CategoryMessageModel>();
	}
}
