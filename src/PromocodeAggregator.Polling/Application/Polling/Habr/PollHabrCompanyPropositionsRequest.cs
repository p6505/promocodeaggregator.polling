using HtmlAgilityPack;
using Mediator;
using Microservices.Kafka.Abstractions;
using Microservices.Kafka.Transfers;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Application.Polling.Abstractions;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Habr;

public record PollHabrCompanyPropositionsRequest(CompanyMessageModel CompanyMessage) : IRequest;

/*
 <div class="coupon clearfix " data-id="3551188">
      <div class="choice"><i class="icon-Tick"></i></div>
    <div class="logo-block">
    <a href="https://promo.habr.com/go/3551188" data-id="3551188" class="popup " rel="nofollow" target="_blank" onclick="hidden">
      <img src="/pkd/uploads/campaigns/1121.jpg" class="brand-logo  " alt="Domino’s Pizza">
    </a>
    
    <div class="rating">
      <div class="negative rating-update" data-positive="0" onclick="ym(window.SLMetrikaId,'reachGoal','coupon-like')"><i class="icon-dislike"></i></div>
      <div class="rate">14</div>
      <div class="positive rating-update" data-positive="1" onclick="ym(window.SLMetrikaId,'reachGoal','coupon-dislike')"><i class="icon-like"></i></div>
    </div>
  </div>
  <div class="text-block">
    <a href="https://promo.habr.com/go/3551188" rel="nofollow" target="_blank" class="title popup  " data-id="3551188" onclick="hidden">
      Для каждого! Экономия 25% при заказе любых блюд по промокоду!</a>
    <time>
      <span class="hidden-title"> Промокод действует </span>
      <span class="shown-title">Действует</span>
              <strong>до 31 мая 2023, 23:59</strong>
          </time>
            <div class="buttons clearfix">
      <a rel="nofollow" data-id="3551188" target="_blank" onclick="hidden">
      <a rel="nofollow" data-id="3551188" target="_blank" href="https://promo.habr.com/go/3551188" class="hidden-code popup "></a>
    </div>
  </div>
</div>
 */

public class Poll : PollBase,
	IRequestHandler<PollHabrCompanyPropositionsRequest>
{
	private readonly ApplicationProducerOptions _applicationProducerOptions;
	private readonly IKafkaProducer<EventKey, PollingResultMessageModel> _producer;
	private readonly ILogger<Poll> _logger;
	private readonly HabrPollingOptions _options;

	public Poll(
		IOptions<HabrPollingOptions> options,
		IOptions<ApplicationProducerOptions> producerOptions,
		IKafkaProducer<EventKey, PollingResultMessageModel> producer,
		ILogger<Poll> logger)
	{
		_producer = producer;
		_logger = logger;
		_options = options.Value;
		_applicationProducerOptions = producerOptions.Value;
	}

	public async ValueTask<Unit> Handle(PollHabrCompanyPropositionsRequest request, CancellationToken cancellationToken)
	{
		await LoadPromoCodes(request.CompanyMessage);

		return Unit.Value;
	}

	private async Task LoadPromoCodes(CompanyMessageModel company)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();

		var companyUrl = company.Source;
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, companyUrl));
		var stringContent = await html.Content.ReadAsStringAsync();
		document.LoadHtml(stringContent);

		var coupons = document.DocumentNode.SelectNodes(
				"//div[contains(@class,'coupon') and following-sibling::h2 and not(contains(@class, 'cross-top'))]")
			?.ToArray() ?? Array.Empty<HtmlNode>();

		var links = coupons.Select(x =>
			x.SelectSingleNode(x.XPath + "//a[contains(@class,'hidden-code')]")?.GetAttributeValue("href", null)
		).Where(x => x != null).ToList();

		if (!links.Any())
		{
			_logger.LogWarning("No promotion links for company {companyUrl}", companyUrl);
			return;
		}

		foreach (var link in links)
		{
			var couponId = new Uri(link).Segments.Last();

			var codeUrl = companyUrl + $"?couponId={couponId}&display_code=1";
			var codeResponse = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, codeUrl));
			var codeHtml = await codeResponse.Content.ReadAsStringAsync();

			document.LoadHtml(codeHtml);

			var propositionValue = GetPromocode(document);
			var propositionKind = PropositionKind.Promocode;

			if (string.IsNullOrWhiteSpace(propositionValue))
			{
				propositionKind = PropositionKind.Promotion;
				propositionValue = GetPromotion(document);
			}

			if (string.IsNullOrWhiteSpace(propositionValue))
			{
				_logger.LogWarning("Can't load propositon for {codeUrl}", codeUrl);
				await Task.Delay(_options.PromotionInterval);
				continue;
			}

			var propositionItem = new PropositionItemMessageModel
			{
				Name = GetName(document),
				Value = propositionValue,
				Link = codeUrl,
				Description = GetDescription(document),
				Expiration = GetExpiration(document),
				PropositionKind = propositionKind
			};

			var resultMessage = new PollingResultMessageModel
			{
				Company = company,
				Propositions = new List<PropositionItemMessageModel> { propositionItem },
				SourceId = (int)Sources.PromoHabr
			};
			var key = new EventKey { Id = Guid.NewGuid() };
			await _producer.Produce(_applicationProducerOptions.PropositionsResultTopic, key, resultMessage);

			await Task.Delay(_options.PromotionInterval);
		}
	}

	private static string GetPromocode(HtmlDocument document) =>
		document.DocumentNode.SelectSingleNode("//div/input")?.GetAttributeValue("value", null)?.Trim();

	private static string GetDescription(HtmlDocument document)
	{
		var nodeValue = document.DocumentNode.SelectSingleNode("//div[contains(@class,'cm-descr cm-visible')]")
			?.GetDirectInnerText().Trim();

		return RemoveExtraSpaces(nodeValue);
	}

	private static DateTimeOffset? GetExpiration(HtmlDocument document)
	{
		var nodeValue = document.DocumentNode.SelectSingleNode("//h5[contains(@class,'cm-subtitle')]//strong")
			?.GetDirectInnerText();

		return ParseExpiration(nodeValue);
	}

	private static string GetName(HtmlDocument document)
	{
		var nodeValue = document.DocumentNode.SelectSingleNode("//div[contains(@class,'cm-head')]//h2")
			?.GetDirectInnerText();

		return RemoveExtraSpaces(nodeValue);
	}

	private static string GetPromotion(HtmlDocument document)
	{
		var nodeValue = document.DocumentNode
			.SelectSingleNode("//div[contains(@class,'cm-descr-warp')]//div[contains(@class,'cm-desc')]")
			?.GetDirectInnerText()?.Trim();

		return RemoveExtraSpaces(nodeValue);
	}
}
