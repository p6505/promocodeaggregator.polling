using HtmlAgilityPack;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Habr;

public record PollHabrCompaniesRequest(CategoryMessageModel Category) : IRequest<List<CompanyMessageModel>>;

/*
<div class="ca-col">
  <a href="https://promo.habr.com/offer/dominos-pizza">
    <img src="/pkd/uploads/campaigns/1121.jpg" alt="Купоны и промокоды Domino’s Pizza" title="Купоны и промокоды Domino’s Pizza">
    <div>Domino’s Pizza</div>
  </a>
</div>
 */
public class HabrPromoPollingRequestHandler : IRequestHandler<PollHabrCompaniesRequest, List<CompanyMessageModel>>
{
	private readonly HabrPollingOptions _options;

	public HabrPromoPollingRequestHandler(IOptions<HabrPollingOptions> options)
	{
		_options = options.Value;
	}

	public async ValueTask<List<CompanyMessageModel>> Handle(
		PollHabrCompaniesRequest request,
		CancellationToken cancellationToken)
	{
		using var httpClient = new HttpClient();
		var document = new HtmlDocument();

		var uri = new Uri(new Uri(_options.BaseUrl), $"categories/{request.Category.Name}");
		var html = await httpClient.SendAsync(new HttpRequestMessage(HttpMethod.Get, uri), cancellationToken);
		var stringContent = await html.Content.ReadAsStringAsync(cancellationToken);
		document.LoadHtml(stringContent);

		var companies = document
			.DocumentNode
			.SelectNodes("//div[@class='ca-col']")
			?.Where(x => x.Name == "div").Select(x => new CompanyMessageModel
			{
				Name = x.SelectSingleNode("a/div")?.GetDirectInnerText(),
				ImageLink = uri + x.SelectSingleNode("a/img")?.GetAttributeValue("src", null),
				ImageBase64 = null,
				Source = x.SelectSingleNode("a")?.GetAttributeValue("href", null),
				Category = request.Category
			})
			.Where(x => x.Source != null && x.Name != null)
			.ToList();

		return companies ?? new List<CompanyMessageModel>();
	}
}
