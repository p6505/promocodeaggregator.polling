﻿using System.Runtime.CompilerServices;
using Mediator;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Broker.Messages;
using PromocodeAggregator.Polling.Extension;
using PromocodeAggregator.Polling.Options;

namespace PromocodeAggregator.Polling.Application.Polling.Habr;

public record PollHabrPropositionsRequest : IRequest;

public class PollHabrPropositionsRequestHandler : IRequestHandler<PollHabrPropositionsRequest>
{
	private const Sources Source = Sources.PromoHabr;

	private readonly IMediator _mediator;
	private readonly HabrPollingOptions _options;
	private readonly ILogger<PollHabrPropositionsRequestHandler> _logger;

	public PollHabrPropositionsRequestHandler(
		IMediator mediator,
		IOptions<HabrPollingOptions> options,
		ILogger<PollHabrPropositionsRequestHandler> logger)
	{
		_mediator = mediator;
		_options = options.Value;
		_logger = logger;
	}

	public async ValueTask<Unit> Handle(PollHabrPropositionsRequest request, CancellationToken cancellationToken)
	{
		_logger.LogSourcePollingFinished(Source);

		var random = new Random();

		await foreach (var companies in GetCompanies(cancellationToken))
		{
			foreach (var company in companies.OrderBy(_ => random.Next()))
			{
				_logger.LogCompanyPollingStarted(Source, company.Category.Name, company.Name);

				await _mediator.Send(new PollHabrCompanyPropositionsRequest(company), cancellationToken);

				_logger.LogCompanyPollingFinished(Source, company.Category.Name, company.Name);

				await Task.Delay(_options.CompanyInterval, cancellationToken);
			}
		}

		_logger.LogSourcePollingFinished(Source);

		return Unit.Value;
	}

	private async IAsyncEnumerable<List<CompanyMessageModel>> GetCompanies(
		[EnumeratorCancellation] CancellationToken cancellationToken)
	{
		Random random = new();

		var categories = await _mediator.Send(new PollHabrCategoriesRequest(), cancellationToken);

		foreach (var category in categories.OrderBy(_ => random.Next()))
		{
			var companies = await _mediator.Send(new PollHabrCompaniesRequest(category), cancellationToken);

			_logger.LogCompaniesReceived(Source, companies.Count, category.Name);

			yield return companies;
		}
	}
}
