﻿using System.Text.RegularExpressions;

namespace PromocodeAggregator.Polling.Application.Polling.Abstractions;

public partial class PollBase
{
	private const int DefaultOffsetHours = 3;
	private const int DefaultSeconds = 0;

	[GeneratedRegex(@"(?<day>\d+)\s+(?<month>[а-я]+)\s+(?<year>\d+)(, (?<hour>\d+):(?<minute>\d+))?")]
	protected static partial Regex ExpirationRegex();

	[GeneratedRegex("\\s{2,}")]
	protected static partial Regex SpacesRegex();

	protected static DateTimeOffset? ParseExpiration(string datetimeOffsetString)
	{
		var regexMatch = ExpirationRegex().Match(datetimeOffsetString);

		if (!regexMatch.Success)
		{
			return null;
		}

		return new DateTimeOffset(
			int.Parse(regexMatch.Groups["year"].ValueSpan),
			GetMonthNumber(regexMatch.Groups["month"].Value) ?? 0,
			int.Parse(regexMatch.Groups["day"].ValueSpan),
			int.TryParse(regexMatch.Groups["hour"].ValueSpan, out var hour) ? hour : 0,
			int.TryParse(regexMatch.Groups["minute"].ValueSpan, out var minute) ? minute : 0,
			DefaultSeconds,
			TimeSpan.FromHours(DefaultOffsetHours));
	}

	protected static string RemoveExtraSpaces(string s) => s != null
		? SpacesRegex().Replace(s.Trim(), " ")
		: null;

	private static int? GetMonthNumber(string monthName) =>
		monthName.ToLower() switch
		{
			"января" => 1,
			"февраля" => 2,
			"марта" => 3,
			"апреля" => 4,
			"мая" => 5,
			"июня" => 6,
			"июля" => 7,
			"августа" => 8,
			"сентабря" => 9,
			"октября" => 10,
			"ноября" => 11,
			"декабря" => 12,
			_ => null
		};
}
