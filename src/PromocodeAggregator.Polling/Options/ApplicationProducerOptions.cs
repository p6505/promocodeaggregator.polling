namespace PromocodeAggregator.Polling.Options;

public class ApplicationProducerOptions
{
	public string PropositionsResultTopic { get; set; }
}
