namespace PromocodeAggregator.Polling.Options;

public class BasePollingOptions
{
	public string BaseUrl { get; set; }

	public TimeSpan PromotionInterval { get; set; }

	public TimeSpan CompanyInterval { get; set; }

	public string JobCron { get; set; }

	public string[] IgnoredCategories { get; set; } = Array.Empty<string>();
}
