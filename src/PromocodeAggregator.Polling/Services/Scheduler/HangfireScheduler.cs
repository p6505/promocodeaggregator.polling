﻿using Hangfire;
using Microsoft.Extensions.Options;
using PromocodeAggregator.Polling.Application.Polling.Lifehacker;
using PromocodeAggregator.Polling.Application.Polling.Promocodus;
using PromocodeAggregator.Polling.Options;
using PromocodeAggregator.Polling.Services.HangfireExecutor;

namespace PromocodeAggregator.Polling.Services.Scheduler;

public class HangfireScheduler : BackgroundService
{
	private readonly IHangfireExecutor _hangfireExecutor;
	private readonly IOptionsMonitor<HabrPollingOptions> _option;

	private readonly TimeSpan _sleepDelay = TimeSpan.FromMinutes(1);

	public HangfireScheduler(IHangfireExecutor hangfireExecutor, IOptionsMonitor<HabrPollingOptions> option)
	{
		_hangfireExecutor = hangfireExecutor;
		_option = option;
	}

	protected override async Task ExecuteAsync(CancellationToken stoppingToken)
	{
		while (!stoppingToken.IsCancellationRequested)
		{
			// RecurringJob.AddOrUpdate(
				// nameof(PollHabrPropositionsRequest),
				// () => _hangfireExecutor.Execute(new PollHabrPropositionsRequest()),
				// _option.CurrentValue.JobCron);
			
			RecurringJob.AddOrUpdate(
				nameof(PollPromocodusPropositionsRequest),
				() => _hangfireExecutor.Execute(new PollPromocodusPropositionsRequest()),
				_option.CurrentValue.JobCron);
			
			RecurringJob.AddOrUpdate(
				nameof(PollLifehackerPropositionsRequest),
				() => _hangfireExecutor.Execute(new PollLifehackerPropositionsRequest()),
				_option.CurrentValue.JobCron);

			// RecurringJob.AddOrUpdate(
				// nameof(PollPromocodiNetPropositionsRequest),
				// () => _hangfireExecutor.Execute(new PollPromocodiNetPropositionsRequest()),
				// _option.CurrentValue.JobCron);

			await Task.Delay(_sleepDelay, stoppingToken);
		}
	}
}
