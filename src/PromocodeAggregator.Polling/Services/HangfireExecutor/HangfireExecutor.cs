﻿using Mediator;

namespace PromocodeAggregator.Polling.Services.HangfireExecutor;

public class HangfireExecutor : IHangfireExecutor
{
	private readonly IServiceProvider _serviceProvider;

	public HangfireExecutor(IServiceProvider serviceProvider)
	{
		_serviceProvider = serviceProvider;
	}

	public async Task Execute(IRequest request)
	{
		using var scope = _serviceProvider.CreateScope();

		var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();

		await mediator.Send(request);
	}
}
