﻿using Mediator;

namespace PromocodeAggregator.Polling.Services.HangfireExecutor;

public interface IHangfireExecutor
{
	Task Execute(IRequest request);
}
