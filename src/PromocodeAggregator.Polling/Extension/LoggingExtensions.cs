﻿using PromocodeAggregator.Polling.Application;

namespace PromocodeAggregator.Polling.Extension;

public static partial class LoggingExtensions
{
	[LoggerMessage(
		1001,
		LogLevel.Information,
		"{Source} Started polling for category {Category} and company {CompanyName}")]
	public static partial void LogCompanyPollingStarted(
		this ILogger logger,
		Sources source,
		string category,
		string companyName);

	[LoggerMessage(
		1002,
		LogLevel.Information,
		"{Source} Finished polling for category {Category} and company {CompanyName}")]
	public static partial void LogCompanyPollingFinished(
		this ILogger logger,
		Sources source,
		string category,
		string companyName);

	[LoggerMessage(
		1003,
		LogLevel.Information,
		"{Source} Finished polling all data")]
	public static partial void LogSourcePollingFinished(this ILogger logger, Sources source);

	[LoggerMessage(
		1004,
		LogLevel.Information,
		"{Source} Received {Count} companies for category {Category}")]
	public static partial void LogCompaniesReceived(
		this ILogger logger,
		Sources source,
		int count,
		string category);

	[LoggerMessage(
		1005,
		LogLevel.Information,
		"{Source} Polling started")]
	public static partial void LogSourcePollingStarted(
		this ILogger logger,
		Sources source);
}
