﻿using Hangfire;
using Hangfire.PostgreSql;
using Newtonsoft.Json;

// ReSharper disable UnusedMethodReturnValue.Global

namespace PromocodeAggregator.Polling.Extension;

public static class HangfireServiceCollectionExtension
{
	public static IServiceCollection AddHangfire(this IServiceCollection services, IConfiguration configuration)
	{
		var connectionString = configuration.GetConnectionString("Hangfire");
		services
			.AddHangfire(options =>
			{
				options.UsePostgreSqlStorage(connectionString);
				options.UseFilter(new AutomaticRetryAttribute());
				options.UseSerializerSettings(new JsonSerializerSettings
				{
					TypeNameHandling = TypeNameHandling.Objects,
				});
			})
			.AddHangfireServer(options =>
			{
				options.WorkerCount = Environment.ProcessorCount * 4;
				options.SchedulePollingInterval = TimeSpan.FromSeconds(5);
			});

		return services;
	}
}
